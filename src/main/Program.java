package main;
import gui.WindowGUI;
import utilitaires.Echiquier;


public class Program {

    public static void main(String[] args) {


    // ---------------------------------------------------------------------------------------------------
    // Lancement du jeu

    // La GUI
    WindowGUI theView = new WindowGUI();

    // Update les listes de tous les coups possibles
    Echiquier.moveConfirmed();

    }
}
