package players;


import ai.Move;
import pieces.PieceEchec;
import pieces.Roi;
import utilitaires.Echiquier;
import java.util.ArrayList;
import java.util.Collection;



public class Player {




    // Variables -----------------------------------------------------------------------------------------------------//
    private String nom;
    // Par convention 1 les blancs et 2 les noirs
    private int couleur = 0;
    private int score = 0;
    private int mooves = 0;
    private boolean hasStarted = false;

    // Le roi du joueur
    private Roi playerKing;

    private Collection<Move> whiteLegalMoves;
    private Collection<Move> blackLegalMoves;
    private Collection<PieceEchec> whitePieces;
    private Collection<PieceEchec> blackPieces;



    // Constructor ---------------------------------------------------------------------------------------------------//
    public Player (String nom, int couleur, boolean hasStarted,
                   Collection whiteLegalMoves, Collection blackLegalMoves, Roi playerKing) {
        this.nom = nom;
        this.couleur = couleur;
        this.score = 0;
        this.mooves = 0;
        this.hasStarted = hasStarted;
        this.whiteLegalMoves = Echiquier.getWhiteLegalMoves();
        this.blackLegalMoves = Echiquier.getBlackLegalMoves();
        this.playerKing = playerKing;
    }

    // Methods -------------------------------------------------------------------------------------------------------//


    // Getters ---------------------
    public String getNom() {
        return nom;
    }
    public int getCouleur() {
        return couleur;
    }
    public int getScore() {
        return score;
    }
    public int getMooves() {
        return mooves;
    }
    public boolean getHasStarted() {
        return hasStarted;
    }
    public Collection<Move> getWhiteLegalMoves() {
        return Echiquier.getWhiteLegalMoves();
    }
    public Collection<Move> getBlackLegalMoves() {
        return Echiquier.getBlackLegalMoves();
    }
    public Roi getPlayerKing() {
        return playerKing;
    }
    public Collection<PieceEchec> getWhitePieces() {
        return Echiquier.getWhitePieces();
    }
    public Collection<PieceEchec> getBlackPieces() {
        return Echiquier.getBlackPieces();
    }

    public Collection<PieceEchec> getActivePieces(){
        if(this.getCouleur() == 1){
            return getWhitePieces();
        }
        else {
            return getBlackPieces();
        }
    }

    public Collection<Move> getLegalMoves(){
        if(this.getCouleur() == 1) {
            return getWhiteLegalMoves();
        }
        else {
            return getBlackLegalMoves();
        }
    }


    // Setters --------------------------------------
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setCouleur(int couleur) {
        this.couleur = couleur;
    }
    public void setHasStarted(boolean hasStarted) {
        this.hasStarted = hasStarted;
    }
    public void setMooves(int mooves) {
        this.mooves = mooves;
    }
    public void setScore(int score) {
        this.score = score;
    }







    public boolean makeMove(PieceEchec[][] echiquier, Move move) {

        boolean isOk = false;
        PieceEchec movedPiece = move.getMovedPiece();
        String destinationCoordz = move.getDestinationCoordz();
        PieceEchec attackedPiece = move.getAttackedPiece();

        int destX = Integer.parseInt(destinationCoordz.split(",")[0]);
        int destY = Integer.parseInt(destinationCoordz.split(",")[1]);

        // Deplacement
        if(echiquier[destX][destY] == null && movedPiece.peutAllerA(destX, destY)){
            echiquier[movedPiece.getPositionX()][movedPiece.getPositionY()] = null;
            echiquier[destX][destY] = movedPiece;
            isOk = true;
        }
        // Ou manger une piece
        else if (echiquier[destX][destY] != null && movedPiece.peutMangerPiece(attackedPiece)) {
            attackedPiece = null;
            echiquier[movedPiece.getPositionX()][movedPiece.getPositionY()] = null;
            echiquier[destX][destY] = movedPiece;
            isOk = true;
        }

        return isOk;
    }








    public String toString() {

        Collection<Move> tMoves = new ArrayList<>();

        if (this.couleur == 1) {
            tMoves = getWhiteLegalMoves();
        }  else {
            tMoves = getBlackLegalMoves();
        }

        return " PLaYeR : " + nom + " Couleur : " + couleur + " First Moove : "
                + hasStarted + " Nb mooves : " + mooves + " Score : "
                + score + "\nMoves possibles : \n" + tMoves.toString();
    }





}
