package gui;


import players.Player;
import sound.Sound;
import utilitaires.Echiquier;
import pieces.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TimerTask;



public class WindowGUI extends JFrame {



    // Variables -------------------------------------------------------------------------------------
    private JButton [][] squares = new JButton[8][8];
    private JPanel panChessBoard = (JPanel) getContentPane();
    // Images
    private ImageIcon imgPionNoir = new ImageIcon(getClass().getResource("/images/pionN.png"));
    private ImageIcon imgTourNoir = new ImageIcon(getClass().getResource("/images/tourN.png"));
    private ImageIcon imgCavalNoir = new ImageIcon(getClass().getResource("/images/cavalierN.png"));
    private ImageIcon imgFouNoir = new ImageIcon(getClass().getResource("/images/fouN.png"));
    private ImageIcon imgReineNoir = new ImageIcon(getClass().getResource("/images/reineN.png"));
    private ImageIcon imgRoiNoir = new ImageIcon(getClass().getResource("/images/roiN.png"));
    private ImageIcon imgPionBlanc = new ImageIcon(getClass().getResource("/images/pionB.png"));
    private ImageIcon imgTourBlanc = new ImageIcon(getClass().getResource("/images/tourB.png"));
    private ImageIcon imgCavalBlanc = new ImageIcon(getClass().getResource("/images/cavalierB.png"));
    private ImageIcon imgFouBlanc = new ImageIcon(getClass().getResource("/images/fouB.png"));
    private ImageIcon imgReineBlanc = new ImageIcon(getClass().getResource("/images/reineB.png"));
    private ImageIcon imgRoiBlanc = new ImageIcon(getClass().getResource("/images/roiB.png"));
    // La piece active en cours d'utilisation
    private PieceEchec pieceActive = null;
    // La piece victime
    private PieceEchec pieceVictim = null;
    // Les joueurs
    private Player player1 = new Player("Player 1", 1,false,
                            Echiquier.getWhiteLegalMoves(), Echiquier.getBlackLegalMoves(),
                            Echiquier.getWhiteKing());  // Blancs
    private Player player2 = new Player("Player 2", 2,false,
                            Echiquier.getBlackLegalMoves(), Echiquier.getWhiteLegalMoves(),
                            Echiquier.getBlackKing());
    // La couleur qui a jouée le last moove
    // Par convention 1 les blancs // 2 les noirs
    private int lastColorPlay = 2;
    private boolean kingCheck = false;
    // Le son
    Sound soundEngine = new Sound();
    // Les timers pour les affichages de pieces selectionnées
    private java.util.Timer t1 = new java.util.Timer();
    private java.util.Timer t2 = new java.util.Timer();
    // Les modes de jeu
    private boolean oneVsOne = true;
    private boolean youVsCpu = false;



    // Constructor -----------------------------------------------------------------------------------
    public WindowGUI(){

        super("MrXroM Che55");
        panChessBoard.setLayout(new GridLayout(8,8));

        // Create event handlers
        CasesListeners eventHandler = new CasesListeners();

        // Place les 64 JButtons ds le tab squares[][] avec les backgrounds et les listeners
        for(int i=0; i< 8; i++) {
            for(int j=0; j<8; j++) {

                squares[i][j] = new JButton();

                if((i+j) % 2 != 0) {
                    squares[i][j].setBackground( new Color(0x7E611F));
                }
                panChessBoard.add(squares[i][j]);
                squares[i][j].addActionListener(eventHandler);
            }
        }
        placePieces();
        initView();
    }



    // Getters ----------------------------------------------------------------------------------------

    public JButton[][] getSquares() {
        return squares;
    }
    public JPanel getPanChessBoard() {
        return panChessBoard;
    }




    // Methods ---------------------------------------------------------------------------------------

    /**
     *  Init window GUI
     */
    private void initView() {
        buildJmenuBar();
        setVisible(true);
        setSize(new Dimension(975,975));
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setBackground(Color.LIGHT_GRAY);
        setTitle("<< MrXroM Che55 >>");
        buildEchiquer();
    }




    /**
     * Construit le tableau de la classe Echiquier
     */
    private void buildEchiquer() {
        // Build l'echiquier avec ses pieces
        Echiquier.buildEchiquier();
        Echiquier.getTheKings();
    }




    /**
     *  Place les imagesIcons des pieces ds chaque cellule du grid layout
     */
    private void placePieces(){
        // Les noirs
        squares[0][0].setIcon(imgTourNoir);
        squares[0][7].setIcon(imgTourNoir);
        squares[0][1].setIcon(imgCavalNoir);
        squares[0][6].setIcon(imgCavalNoir);
        squares[0][2].setIcon(imgFouNoir);
        squares[0][5].setIcon(imgFouNoir);
        squares[0][3].setIcon(imgReineNoir);
        squares[0][4].setIcon(imgRoiNoir);
        // Loop pour placer les pions noirs
        for(int i=0; i<8; i++) {
            squares[1][i].setIcon(imgPionNoir);
        }

        // Les blancs
        squares[7][0].setIcon(imgTourBlanc);
        squares[7][7].setIcon(imgTourBlanc);
        squares[7][1].setIcon(imgCavalBlanc);
        squares[7][6].setIcon(imgCavalBlanc);
        squares[7][2].setIcon(imgFouBlanc);
        squares[7][5].setIcon(imgFouBlanc);
        squares[7][3].setIcon(imgReineBlanc);
        squares[7][4].setIcon(imgRoiBlanc);
        // pions blancs
        for(int i=0; i<8; i++) {
            squares[6][i].setIcon(imgPionBlanc);
        }
    }




    /**
     * Build le menu avec les listeners
     */
    private void buildJmenuBar() {

        // -------------------------------------------------------//
        JMenuBar menuBar = new JMenuBar();
        JMenu menuJeu = new JMenu("< Choose Game >");
        JMenuItem jeuOneVsOne = new JMenuItem("One Vs One");
        JMenuItem jeuOneVsCpu = new JMenuItem("You Vs Cpu");
        JMenuItem quit = new JMenuItem("Quit");
        menuJeu.add(jeuOneVsOne);
        menuJeu.add(jeuOneVsCpu);
        menuJeu.add(quit);
        JMenu menuOptions = new JMenu(" < Options > ");
        JMenuItem reset = new JMenuItem("Reset game");
        menuOptions.add(reset);
        JMenu menuApropos = new JMenu(" < ? >");
        JMenuItem about = new JMenuItem("? About ?");
        menuApropos.add(about);
        menuBar.add(menuJeu);
        menuBar.add(menuOptions);
        menuBar.add(menuApropos);
        // Set menu bar
        setJMenuBar(menuBar);

        // Listeners ------------------------------------------ //
        // One Vs One
        jeuOneVsOne.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                oneVsOne = true;
                youVsCpu = false;
                JOptionPane.showMessageDialog(panChessBoard, "Ok", "Let's play che55",
                        JOptionPane.WARNING_MESSAGE);
                // Ici les futures instructions pour selectionner le mode One Vs One

            }
        });
        // You Vs Cpu
        jeuOneVsCpu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                oneVsOne = false;
                youVsCpu = true;
                JOptionPane.showMessageDialog(panChessBoard, "Feature in developpment",
                        "WiLL WorK SooN", JOptionPane.WARNING_MESSAGE);
                // Ici les futures instructions pour selectionner le mode You Vs Cpu

            }
        });
        // Reset Game
        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetGame();
            }
        });
        // Quitter
        quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        // About
        about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(panChessBoard, "MrXroM 2019",
                        "MrXroM Che55", JOptionPane.WARNING_MESSAGE);
            }
        });
    }







    /**
     * Reset tt le jeu ds le Gui et l'echiquier
     */
    private void resetGame() {
        // Ds le Gui
        // Clean l'echiquier
        for(int i=0; i<8;i++){
            for(int j=0; j<8;j++) {
                squares[i][j].setIcon(null);
            }
        }
        placePieces();
        buildEchiquer();
        // Les variables
        lastColorPlay = 0;
        kingCheck = false;
        pieceActive = null;
        pieceVictim = null;
        player1.setScore(0);
        player2.setScore(0);
        player1.setMooves(0);
        player2.setMooves(0);
    }







    /**
     * Permet le clignotement de la case de la piece active
     * @param i
     * @param j
     */
    private void togglePieceSelected(int i, int j) {

        Color color;
        // Les timers pour afficher la piece active en toggle
        t1 = new java.util.Timer();
        t2 = new java.util.Timer();

        t1.schedule(new TimerTask() {
            @Override
            public void run() {
                squares[i][j].setBackground(Color.darkGray);
            }
        }, 500, 500);

        if(Echiquier.getCouleurCase(j,i) == 1) {
            color = new Color(0xDEE9F7);
        }
        else {
            color = new Color(0xDEE9F7);
        }
        t2.schedule(new TimerTask() {
            @Override
            public void run() {
                squares[i][j].setBackground(color);
            }
        }, 1000, 1000);
    }






    /**
     * Remet la bonne couleur sur la case de la piece active qui clignote
     * @param i coord X du click
     * @param j coord Y du click
     */
    private void replaceColors(int i , int j) {
        // case noire
        if(Echiquier.getCouleurCase(j,i) == 2){
            squares[i][j].setBackground( new Color(0x7E611F));
        }
        else if (Echiquier.getCouleurCase(j,i) == 1){
            // Case blanche
            squares[i][j].setBackground(new Color(0xDEE9F7));
        }
    }




    /**
     * Execute le roque sur le GUI et l'echiquier
     * @param roi le roi qui roque
     * @param xD coord X
     * @param yD coord Y
     */
    private void doTheRook(PieceEchec roi, int xD, int yD) {

        // On GUI
        Icon iconeRoi = squares[roi.getPositionY()][roi.getPositionX()].getIcon();
        Icon iconeTour = null;
        // Ds le GUI
        if(xD == 7 && yD == 6){         // Petit Rook blanc
            iconeTour = squares[7][7].getIcon();
            squares[roi.getPositionY()][roi.getPositionX()].setIcon(null);
            squares[xD][yD].setIcon(iconeRoi);
            squares[7][7].setIcon(null);
            squares[7][5].setIcon(iconeTour);
        }
        else if(xD == 7 && yD == 2) {   // Grand rook blanc
            iconeTour = squares[7][0].getIcon();
            squares[roi.getPositionY()][roi.getPositionX()].setIcon(null);
            squares[xD][yD].setIcon(iconeRoi);
            squares[7][0].setIcon(null);
            squares[7][3].setIcon(iconeTour);
        }

        else if(xD == 0 && yD == 6){    // Petit rook noir
            iconeTour = squares[0][7].getIcon();
            squares[roi.getPositionY()][roi.getPositionX()].setIcon(null);
            squares[xD][yD].setIcon(iconeRoi);
            squares[0][7].setIcon(null);
            squares[0][5].setIcon(iconeTour);
        }
        else if(xD == 0 && yD == 2){     // Grand rook noir
            iconeTour = squares[0][0].getIcon();
            squares[roi.getPositionY()][roi.getPositionX()].setIcon(null);
            squares[xD][yD].setIcon(iconeRoi);
            squares[0][0].setIcon(null);
            squares[0][3].setIcon(iconeTour);
        }

        // Et ds l'Echiquier
        Echiquier.doTheRook(roi, yD, xD);
    }





    /**
     * Check si une piece est active (deja cliquée) et la couleur pour son tour
     * Check si une piece est présente sur la case du click
     * Si la piece active peut aller à l'endroit x,y du click
     * ou Si la piece active peut manger la piece adverse
     * @param i Coordonnée X du click
     * @param j Coordonnée Y du click
     */
    private void doTheMoove(int i, int j) {

        // Une piece est selectionnée // Et la couleur de la PieceActiV n'est pas la last played (set à 1 au départ)
        if(pieceActive != null && (lastColorPlay != pieceActive.getCouleurPiece()) ) {

            // Remet la bonne couleur sur la case
            t1.cancel();
            t2.cancel();
            replaceColors(pieceActive.getPositionY(),pieceActive.getPositionX());

            // Pour aller quelque part --------------------------------------------------------------------------------
            // Aucune piece est presente
            if(Echiquier.getPieceAt(j,i) == null) {

                if(pieceActive.peutAllerA(j,i)) {

                    // En cas de roque
                    if(pieceActive.getClass().getName().equals("pieces.Roi")
                        && Math.abs(pieceActive.getPositionX() - j) == 2
                        && pieceActive.getPositionX() == 4
                        && ( pieceActive.getPositionY() == 7
                        || pieceActive.getPositionY() == 0) ) {
                        doTheRook(pieceActive, i, j);
                        soundEngine.playSound("/sound/moove.wav");
                        Echiquier.incrementCounters(pieceActive);
                        lastColorPlay = pieceActive.getCouleurPiece();
                        pieceActive = null;
                        return;
                    }

                    // Deplace la piece sur le GUI ---------------------------------
                    Icon icone = squares[pieceActive.getPositionY()][pieceActive.getPositionX()].getIcon();
                    squares[i][j].setIcon(icone);
                    squares[pieceActive.getPositionY()][pieceActive.getPositionX()].setIcon(null);


                    // Deplace la piece dans l'echiquier --------------------------
                    Echiquier.dotheMove(Echiquier.getEchiquier(), pieceActive, j, i);

                    // Set last color played

                    lastColorPlay = pieceActive.getCouleurPiece();

                    // Reset la piece active
                    pieceActive = null;

                    // Update les listes de tous les coups possibles
                    Echiquier.moveConfirmed();

                    // Son déplacemeent
                    soundEngine.playSound("/sound/moove.wav");

                    if(Echiquier.isInCheck(Echiquier.getEchiquier()) != 0) {
                        soundEngine.playSound("/sound/check.wav");
                    }
                    // Si Echec et mat Game OVer
                    if(Echiquier.aKingIsCheckMate(Echiquier.getEchiquier()) == 1
                        || Echiquier.aKingIsCheckMate(Echiquier.getEchiquier()) == 2) {
                        soundEngine.playSound("/sound/checkmate.wav");
                        JOptionPane.showMessageDialog(panChessBoard, "Echec et mat", "VICTOIRE", JOptionPane.WARNING_MESSAGE);
                    }
                }
                else {
                    // Reset la piece active
                    t1.cancel();
                    t2.cancel();
                    replaceColors(pieceActive.getPositionY(),pieceActive.getPositionX());
                    pieceActive = null;
                }
            }

            // Pour manger une piece ----------------------------------------------------------------------------------
            // Une piece est presente
            else {
                pieceVictim = Echiquier.getPieceAt(j,i);

                // La piece active peut manger la victime
                if (pieceActive.peutMangerPiece(pieceVictim)) {

                    // Le moove sur le GUI -------------------------------------
                    // Supprime l'ancienne piece adverse
                    squares[i][j].setIcon(null);
                    Icon icone = squares[pieceActive.getPositionY()][pieceActive.getPositionX()].getIcon();
                    squares[pieceActive.getPositionY()][pieceActive.getPositionX()].setIcon(null);
                    squares[i][j].setIcon(icone);

                    // Le move dans l'echiquier --------------------------
                    Echiquier.dotheMove(Echiquier.getEchiquier(), pieceActive, j, i);

                    // Set last color played
                    lastColorPlay = pieceActive.getCouleurPiece();

                    // Reset la piece active
                    pieceActive = null;

                    // Update les listes de tous les coups possibles
                    Echiquier.moveConfirmed();

                    // Son mange piece
                    soundEngine.playSound("/sound/eat.wav");

                    // Si Echec et mat Game OVer
                    if(Echiquier.aKingIsCheckMate(Echiquier.getEchiquier()) == 1
                        || Echiquier.aKingIsCheckMate(Echiquier.getEchiquier()) == 2) {
                        soundEngine.playSound("/sound/checkmate.wav");
                        JOptionPane.showMessageDialog(panChessBoard, "Echec et mat", "VICTOIRE", JOptionPane.WARNING_MESSAGE);
                    }
                    if(Echiquier.isInCheck(Echiquier.getEchiquier()) != 0) {
                        soundEngine.playSound("/sound/check.wav");
                    }

                } else {
                    pieceActive = null;
                }
            }
        }
        else {
            t1.cancel();
            t2.cancel();
            replaceColors(i,j);
            pieceActive = null;
        }
    }






    /**
     * Methode executée après un click sur une case de l'echiquer avec une piece active
     * 2 cas roi échec ou non
     * @param i Coordonnée X du click
     * @param j Coordonnée Y du click
     */
    private void processClick(int i, int j) {

        // Roi non echec ------------------------------------------------
        if (! kingCheck) {

            // On invalide le moove si il met echec notre couleur qu'on moove
            // Tout sauf le roi déja traité ds peutAllerA
            if(pieceActive != null && ! pieceActive.getClass().getName().equals("pieces.Roi")
                && Echiquier.checkGoodMoove(Echiquier.getEchiquier(), pieceActive, j, i) == pieceActive.getCouleurPiece() ) {

                replaceColors(pieceActive.getPositionY(),pieceActive.getPositionX());
                t1.cancel();
                t2.cancel();
                pieceActive = null;
            }
            else {
                doTheMoove(i,j);
            }

        } else {
            // un king est echec ----------------------------------------
            if(pieceActive != null) {
                // On test le moove. Encore echec on invalide le moove
                if(Echiquier.checkGoodMoove(Echiquier.getEchiquier(), pieceActive, j, i) == 1
                        || Echiquier.checkGoodMoove(Echiquier.getEchiquier(), pieceActive, j, i) == 2) {
                    replaceColors(pieceActive.getPositionY(),pieceActive.getPositionX());
                    t1.cancel();
                    t2.cancel();
                    pieceActive = null;
                }
                else {
                    // Move valide : le roi n'est plus echec après le test
                    doTheMoove(i,j);
                    kingCheck = false;
                }
            }
        }
    }






    /**
     *  Inner class pour les listeners de clicks sur l'echiquier
     *  La source du click case i,j
     *  Check si piece présente
     *  Si oui elle devient la piece active et on sort
     *  Si on a deja un piece active on exectute prcessClick(i,j)
     */
    public class CasesListeners implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            // Loop ttes les cases
            for(int i=0; i<8; i++){
                for(int j=0; j<8; j++){

                    // La source i,j du click
                    if(e.getSource() == squares[i][j]) {

                        // Ds le cas ou un roi est echec
                        if(Echiquier.isInCheck(Echiquier.getEchiquier()) != 0) {
                            kingCheck = true;
                        }
                        // Si une piece sur la case elle devient la piece active
                        if(Echiquier.getPieceAt(j,i) != null && pieceActive == null) {

                            // Cas principal
                            pieceActive = Echiquier.getPieceAt(j,i);

                            // Select visuel
                            if(pieceActive.getCouleurPiece() != lastColorPlay){
                                togglePieceSelected(i,j);
                            }
                            // On sort pour ne pas executer process click (On a juste selectionné notre piece Active)
                            return;
                        }
                        // Deja une piece active on execute processClick(i,j)
                        processClick(i,j);
                        return;
                    }
                }
            }
        }

    } // End class Listener






} // end class GUI

