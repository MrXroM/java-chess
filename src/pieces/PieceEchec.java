package pieces;
import ai.Move;
import utilitaires.Echiquier;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class PieceEchec {



    // Données membres -----------------------------------------------------------------------------------------------//
    private int positionX = 0;
    private int positionY = 0;
    private int couleurPiece = 1;   // Convention 1 blanche et 2 noire


    // Constructor ---------------------------------------------------------------------------------------------------//
    public PieceEchec (int positionX, int positionY, int couleurPiece) {

        // Piece position
        this.positionX = rndPositionToValid(positionX);
        this.positionY = rndPositionToValid(positionY);

        // Check piece couleur
        if(couleurPiece == 1 || couleurPiece == 2) {
            this.couleurPiece = couleurPiece;
        }
        else {
            JOptionPane.showMessageDialog(null, "Invalid couleur");
        }
    }



    // Getters -------------------------------------------------------------------------------------------------------//
    public int getCouleurPiece() {
        return couleurPiece;
    }
    public int getPositionX() {
        return positionX;
    }
    public int getPositionY() {
        return  positionY;
    }


    // Setters -------------------------------------------------------------------------------------------------------//
    public void setPositionX(int positionX) {
        if(positionX >= 0 && positionX < 8) {
            this.positionX = positionX;
        } else {
            JOptionPane.showMessageDialog(null, "Position X incorrecte");
        }
    }

    public void setPositionY(int positionY) {
        if(positionY >= 0 && positionY < 8) {
            this.positionY = positionY;
        } else {
            JOptionPane.showMessageDialog(null, "Position Y incorrecte");
        }
    }

    public void setCouleurPiece(int couleurPiece) {
        if(couleurPiece == 1 || couleurPiece == 2) {
            this.couleurPiece = couleurPiece;
        } else {
            JOptionPane.showMessageDialog(null, "Couleur incorrecte");
        }
    }



    // Private methods -----------------------------------------------------------------------------------------------//


    /**
     * Arronndi une position X ou Y au plus proche
     * @param position
     * @return int la position
    */
    private int rndPositionToValid(int position) {
        if (position < 0) {
            return 0;
        }
        else if (position > 7) {
            return 7;
        }
        else {
            return position;
        }
    }




    /**
     * Check si le moove du fou est possible
     * @param pe La piece en cours d'utilisation
     * @param xD L'endroit X ou elle veut aller
     * @param yD L'endroit Y ou elle veut aller
     * @return  True si moove OK
     */
    private boolean mooveFouPossible(PieceEchec pe, int xD, int yD){

        // Position
        int currentPosX = pe.positionX;
        int currentPosY = pe.positionY;
        // Déplacement
        int deplacementX = xD - currentPosX;
        int deplacementY = yD - currentPosY;


        // Check for diagonal moove
        if(Math.abs(currentPosX - xD) == Math.abs(currentPosY - yD)) {

            // Controle si aucune piece sur le chemin
            // Passer les cases une par une et tester
            if(deplacementX > 0 && deplacementY < 0) {      // MooV en haut a droite -----------------------------------
                for(int x = this.positionX +1, y = this.positionY -1; x < xD; x++, y-- ) {
                    // Une piece est présente, on sort
                    if(Echiquier.getPieceAt(x, y) != null) {
                        return false;
                    }
                }
                // Fin de loop aucune sortie false, on sort a true
                return true;
            }
            if(deplacementX < 0 && deplacementY > 0) {      // MooV en bas a gauche ------------------------------------
                for(int x = this.positionX -1, y = this.positionY +1; y < yD; x--, y++ ) {
                    // Une piece est présente, on sort
                    if(Echiquier.getPieceAt(x, y) != null) {
                        return false;
                    }
                }
                return true;
            }
            if(deplacementX > 0 && deplacementY > 0) {      // MooV en bas a droite ------------------------------------
                for(int x = this.positionX +1, y = this.positionY +1; y < yD; x++, y++ ) {
                    // Une piece est présente, on sort
                    if(Echiquier.getPieceAt(x, y) != null) {
                        return false;
                    }
                }
                return true;
            }
            if(deplacementX < 0 && deplacementY < 0) {      // MooV en Haut a gauche -----------------------------------
                for(int x = this.positionX -1, y = this.positionY -1; y > yD; x--, y-- ) {
                    // Une piece est présente, on sort
                    if(Echiquier.getPieceAt(x, y) != null) {
                        return false;
                    }
                }
                return true;
            }
        }
        // Problem avec des pieces qui genent le moove
        return false;
    }




    /**
     * Check si le moove de la tour est possible
     * @param pe La piece en cours d'utilisation
     * @param xD L'endroit X ou elle veut aller
     * @param yD L'endroit Y ou elle veut aller
     * @return  True si moove OK
     */
    private boolean mooveTourPossible(PieceEchec pe, int xD, int yD) {

        int currentPosY = pe.positionY;
        int currentPosX = pe.positionX;
        // Déplacement
        int deplacementX = xD - currentPosX;
        int deplacementY = yD - currentPosY;

        // Une des values doit etre la mm
        if(xD == currentPosX || yD == currentPosY) {

            // Aucune piece ne fait obstacle
            if(deplacementX > 0 && deplacementY == 0) {      // MooV a droite ------------------------------------------

                for(int x = this.positionX +1; x < xD; x++) {
                    // Une piece est présente, on sort
                    if (Echiquier.getPieceAt(x, this.getPositionY()) != null) {
                        return false;
                    }
                }
                // Fin de loop aucune sortie false, on sort avec un send a true
                return true;
            }
            if(deplacementX < 0 && deplacementY == 0) {      // MooV a gauche ------------------------------------------

                for(int x = this.positionX -1; x > xD; x--) {
                    // Une piece est présente, on sort
                    if (Echiquier.getPieceAt(x, this.getPositionY()) != null) {
                        return false;
                    }
                }
                return true;
            }
            if(deplacementX == 0 && deplacementY < 0) {      // MooV en haut -------------------------------------------

                for(int y = this.positionY -1; y > yD; y--) {
                    // Une piece est présente, on sort
                    if (Echiquier.getPieceAt(this.getPositionX(), y) != null) {
                        return false;
                    }
                }
                return true;
            }
            if(deplacementX == 0 && deplacementY > 0) {      // MooV en bas --------------------------------------------
                for(int y = this.positionY +1; y < yD; y++) {
                    // Une piece est présente, on sort
                    if (Echiquier.getPieceAt(this.getPositionX(), y) != null) {
                        return false;
                    }
                }
                return true;
            }
        }
        // Problem avec des pieces qui genent le moove
        return false;
    }






    /**
     * Valide le moove du roi N / S / E / W d'une case
     * Et aucune piece adverse le met echec là ou il veut aller
     * @param pe la piece
     * @param xD coordonnée X voulue
     * @param yD coordonnée Y voulue
     * @return true si moove Ok
     */
    public boolean mooveRoi(PieceEchec pe, int xD, int yD) {
        if ((Math.abs(pe.getPositionX() - xD) == 1  && pe.getPositionY() == yD) ||
                (Math.abs(pe.getPositionY() - yD) == 1 && pe.getPositionX() == xD) ||
                (Math.abs(pe.getPositionX() - xD) == 1 && Math.abs(pe.getPositionY() - yD) == 1) ) {

            // Check ttes les pieces adverses avec peut manger king a sa nouvelle position
                PieceEchec roi = new Roi(xD, yD, this.getCouleurPiece());

                for(int i=0; i<8; i++){
                    for(int j=0; j<8; j++) {
                        PieceEchec pieceAdverse = Echiquier.getPieceAt(i,j);
                        if(pieceAdverse != null && pieceAdverse.getCouleurPiece() != this.getCouleurPiece()
                                && pieceAdverse.peutMangerPiece(roi)) {
                            // Une piece suffit on sort a false
                            return false;
                        }
                    }
                }
                // Moove ok
            return true;
            }
        return false;
    }





    /**
     * Methode qui check la classe de la piece envoyée en param
     * Check si la piece peut faire le moove en question
     * @param xD
     * @param yD
     * @return true si OK
    */
    public boolean peutAllerA(int xD, int yD) {

        // Get current position
        int currentPosX = this.getPositionX();
        int currentPosY = this.getPositionY();


        // Check for No Outside values given
        if(xD >= 0 && xD < 8 && yD >= 0 && yD < 8) {

            // Determine la class de la piece
            // Fou ----------------------------------------------------------------------------------
            if(this.getClass().getName().compareToIgnoreCase("pieces.Fou") == 0) {
                if(mooveFouPossible(this, xD, yD)){
                    return true;
                }
            }
            // Tour ---------------------------------------------------------------------------------
            else if(this.getClass().getName().compareToIgnoreCase("pieces.Tour") == 0) {
                if(mooveTourPossible(this, xD, yD)){
                    return true;
                }
            }
            // Cavalier -----------------------------------------------------------------------------
            //  (le + simple)
            else if(this.getClass().getName().compareToIgnoreCase("pieces.Cavalier") == 0) {
                // Cavalier moove
                if( (Math.abs(currentPosX - xD) == 1 && Math.abs(currentPosY - yD) == 2) ||
                        (Math.abs(currentPosX - xD) == 2 && Math.abs(currentPosY - yD) == 1) ) {
                    return true;
                }
            }
            // Reine --------------------------------------------------------------------------------
            else if(this.getClass().getName().compareToIgnoreCase("pieces.Reine") == 0) {
                // Comme une tour Ou un fou
                if(mooveTourPossible(this, xD, yD) || mooveFouPossible(this, xD, yD)) {
                    return true;
                }
            }
            // Roi ----------------------------------------------------------------------------------
            else if(this.getClass().getName().compareToIgnoreCase("pieces.Roi") == 0) {
                // Bouge d'une case a gauche / droite ou en haut / bas ou en diago
                if( mooveRoi(this, xD, yD) ) {
                    // Check si une piece est présente
                    if(Echiquier.getPieceAt(xD,yD) != null) {
                            return false;
                        }
                    // Ok moove validé
                    return true;
                }
                // En cas de rook
                if( (this.getPositionX() == 4 && xD == 6 )|| (this.getPositionX() == 4 && xD == 2) ) {
                    if(Echiquier.isRookPossible(Echiquier.getEchiquier(), this, xD, yD)) {
                        return true;
                    }
                }
            }
            // Les pions ----------------------------------------------------------------------------
            else if(this.getClass().getName().compareToIgnoreCase("pieces.Pion") == 0) {
                // Au tout départ chq couleur peut avancer de 1 ou de 2
                // Les blancs
                if(this.getCouleurPiece() == 2 && currentPosY == 1) {
                    if( (yD == 2 || yD == 3) &&  xD == currentPosX) {
                        return true;
                    }
                }
                // Les noirs
                if(this.getCouleurPiece() == 1 && currentPosY == 6) {
                    if( (yD == 4 || yD == 5) &&  xD == currentPosX) {
                        return true;
                    }
                }
                // Moove normal
                // Check si la case ou aller est occupée
                // Check la couleur et le moove si ok return true
                if(Echiquier.getPieceAt(xD, yD) == null &&
                    (this.getCouleurPiece()== 1 &&currentPosY - yD == 1  && (currentPosX == xD)) ||
                    (this.getCouleurPiece() == 2 && yD - currentPosY == 1  && (currentPosX == xD)) ) {
                    return true;
                }
            }
            else {
                return false;
            }
        }
        return false;
    }





    /**
     * Check la class de la piece en cours
     * Check si elle peut manger la Piece pe selon sa couleur et leur 2 positions
     * @param pe la piece a manger
     * @return true si Ok
     */
    public boolean peutMangerPiece (PieceEchec pe) {

        int deplacementX = pe.getPositionX() - this.getPositionX();

        // Piece d'une autre couleur only !
        if(pe.getCouleurPiece() != this.couleurPiece) {

            // Pour le Cavalier
            if(this.getClass().getName().compareToIgnoreCase("pieces.Cavalier") == 0) {
                // Moove du cavalier
                if(this.peutAllerA(pe.getPositionX(), pe.getPositionY())) {
                    return true;
                }
            }
            // Pour le fou
            else if(this.getClass().getName().compareToIgnoreCase("pieces.Fou") == 0) {
                // Moove en diag du fou.
                if(this.peutAllerA(pe.getPositionX(), pe.getPositionY())) {
                    return true;
                }
            }

            // La tour
            else if(this.getClass().getName().compareTo("pieces.Tour") == 0) {
                if(this.peutAllerA(pe.getPositionX(), pe.getPositionY())) {
                    return true;
                }
            }

            // La reine
            else if(this.getClass().getName().compareTo("pieces.Reine") == 0) {
                if(this.peutAllerA(pe.getPositionX(), pe.getPositionY())) {
                    return true;
                }
            }

            // Le roi
            else if(this.getClass().getName().compareTo("pieces.Roi") == 0) {
                if( Echiquier.getPieceAt(pe.getPositionX(), pe.getPositionY()) != null
                    && mooveRoi(this, pe.getPositionX(), pe.getPositionY()) ) {
                    return true;
                }
            }

            // Le pion
            else if(this.getClass().getName().compareTo("pieces.Pion") == 0) {

                // Blanc bouF Noir ---------------------------------------------
                if(this.getCouleurPiece() == 1 && pe.getCouleurPiece() == 2) {
                    // Victim en Haut / a droite
                    if(deplacementX > 0) {
                        if(this.getPositionX() == pe.getPositionX() -1 && this.getPositionY() == pe.getPositionY() +1) {
                            return true;
                        }
                    }
                    // Victim en Haut / a gauche
                    if(deplacementX < 0) {
                        if(this.getPositionX() == pe.getPositionX() +1 && this.getPositionY() == pe.getPositionY() +1) {
                            return true;
                        }
                    }
                }
                // Noir BouFF Blanc ---------------------------------------------
                if(this.getCouleurPiece() == 2 && pe.getCouleurPiece() == 1) {
                    // Victim en bas / a droite
                    if(deplacementX > 0) {
                        if(this.getPositionX() == pe.getPositionX() -1 && this.getPositionY() == pe.getPositionY() -1) {
                            return true;
                        }
                    }
                    // Victim en bas / a gauche
                    if(deplacementX < 0) {
                        if(this.getPositionX() == pe.getPositionX() +1 && this.getPositionY() == pe.getPositionY() -1) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }





    /**
     * Calculate the list of legal moves for a piece
     * @return Array list of coordz formatted to String : x,y
     */
    public Collection<Move> calculateLegalMoves() {

        final List<Move> legalMoves = new ArrayList<>();

        // Parcours de tt l'echiquier
        for(int i=0; i<8; i++) {
            for(int j=0; j<8; j++) {

                // Format coordz
                String strDest = i + "," + j;

                // Deplacement
                if(Echiquier.getEchiquier()[i][j] == null
                        && this.peutAllerA(i,j)){

                    // Add the move to list
                    legalMoves.add(new Move(this, strDest));

                }
                // Manger une piece
                else if(Echiquier.getEchiquier()[i][j] != null
                        && this.peutMangerPiece(Echiquier.getPieceAt(i,j))) {

                    // Add the move to list
                    legalMoves.add(new Move(this, strDest, Echiquier.getPieceAt(i,j)));
                }
            }
        }

System.out.println(legalMoves.toString());

        return legalMoves;
    }





    /**
     * Assign value for a piece
     * Will be use for ia part
     * @return int value of the piece
     */
    public int getPieceValue(){
        int pieceValue = 0;

        if(this.getClass().getName().equals("pieces.Pion"))
            pieceValue = 100;
        else if(this.getClass().getName().equals("pieces.Cavalier"))
            pieceValue = 300;
        else if(this.getClass().getName().equals("pieces.Fou"))
            pieceValue = 300;
        else if(this.getClass().getName().equals("pieces.Tour"))
            pieceValue = 500;
        else if(this.getClass().getName().equals("pieces.Reine"))
            pieceValue = 900;
        else if(this.getClass().getName().equals("pieces.Roi"))
            pieceValue = 10000;


        return pieceValue;
    }






    // To String
    @Override
    public String toString(){
        return " Piece : " + this.getClass().getName().substring(7)
                + " Position X = " + this.positionX + " - Position Y = "
                + this.positionY + " -- Couleur : " + this.couleurPiece;
    }


}
