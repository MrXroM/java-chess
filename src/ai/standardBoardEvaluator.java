package ai;



import pieces.PieceEchec;
import players.Player;
import utilitaires.Echiquier;
import java.util.Collection;




/**
 * Donne le score du board.
 * For now : La valeur des blancs - la valeur des noirs
 */
public final class standardBoardEvaluator implements BoardEvaluator {



    // Properties ----------------------------------------------------------------------------------------------------//

    private static final int CHECK_BONUS = 50;
    private static final int CHECK_MATE_BONUS = 10000;
    private static final int DEPTH_BONUS = 100;


    // Private methods -----------------------------------------------------------------------------------------------//

    /**
     * Get the score for that board
     *
     * Get the score from white and substract it form the score form blacks
     * Si les blancs ont l'avantage le score sera positif, si les noirs il sera negatif
     * @param board
     * @param depth
     * @return
     */
    @Override
    public int evaluate(PieceEchec [][] board, int depth) {

        return scorePlayer(board, Echiquier.getPlayer1(), depth) -
                scorePlayer(board, Echiquier.getPlayer2(), depth);
    }



    /**
     * Pour l'instant on va baser l'evaluation
     *  sur la somme des values des pieces que le joueur a
     *  et sur la mobilité + le check et le check mate
     * @param board le board
     * @param player player
     * @param depth profondeur
     * @return int score
     */
    private int scorePlayer(PieceEchec [][] board, Player player, int depth) {
        return pieceValue(player) + mobility(player) + check(player, board) + checkMate(board, player, depth);
    }   // + check, checkmate, mobility, ...





    /**
     * Calcul le score du joueur passé en param basé sur la somme de la valeur de ses pieces restantes.
     * @param player
     * @return
     */
    private static int pieceValue (Player player) {
        int pieceValueScore = 0;

        for (PieceEchec piece : player.getActivePieces()) {
            pieceValueScore += piece.getPieceValue();
        }
        return pieceValueScore;
    }




    /**
     * Returns mobility : the nb of possible moves
     * @param player player
     * @return nb for mobility
     */
    private static int mobility(Player player) {
        return player.getLegalMoves().size();
    }



    /**
     * Returns bonus value for check
     * @param player player
     * @return int value
     */
    private static int check(Player player, PieceEchec[][]board) {
        int res = 0;
        if(player.getCouleur() == 1 && Echiquier.isInCheck(board) == 2) {
            res = CHECK_BONUS;
        }
        else if(player.getCouleur() == 2 && Echiquier.isInCheck(board) == 1) {
            res = CHECK_BONUS;
        }
        return res;
    }



    /**
     * Returns bonus value for check mate
     * @param player
     * @return
     */
    private int checkMate(PieceEchec [][] board, Player player, int depth) {
        int res = 0;
        if(player.getCouleur() == 1 && Echiquier.aKingIsCheckMate(board) == 2) {
            res = CHECK_MATE_BONUS * depthBonus(depth);
        }
        else if(player.getCouleur() == 2 && Echiquier.aKingIsCheckMate(board) == 1) {
            res = CHECK_MATE_BONUS * depthBonus(depth);
        }
        return res;
    }



    /**
     * Returns le depth bonus pr trouver plus vite le check mate
     * @param depth
     * @return
     */
    private static int depthBonus(int depth){
        return depth == 0 ? 1 : DEPTH_BONUS * depth;
    }



}
