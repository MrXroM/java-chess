package ai;


import pieces.PieceEchec;
import utilitaires.Echiquier;




/**
 * Va servir à évaluer le board avec la methode evaluate()
 *
 */
public interface BoardEvaluator {


    /**
     * Plus le nombre retourné est élevé + cela avantage les blancs et inversement pour les noirs.
     * @param board
     * @param depth
     * @return
     */
    int evaluate(PieceEchec[][] board, int depth);

}
