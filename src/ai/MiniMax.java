package ai;


import pieces.PieceEchec;
import players.Player;
import utilitaires.Echiquier;



/**
 *
 * On va simuler tous les coups possibles pour chacune de nos pieces et on va scorer chaque coup avant de l'annuler
 *
 *
 * L'algorithme en arbre représente toutes les évolutions possibles du jeu à partir de la postion actuelle (tte en haut)
 *
 *
 *
 * L'algo MinMax est co récursif. Au départ on évalue le board en partant du haut (premiers coups possibles).
 * Puis on descend à une profondeur définie pour calculer le meilleur score donc le meilleur move.
 * Puis on propage ce score cette fois vers le haut et tout en haut.
 *
 *
 *
 *
 */
public class MiniMax implements MoveStrategy {


    // Properties ----------------------------------------------------------------------------------------------------//

    private final BoardEvaluator boardEvaluator;



    // Constructor ---------------------------------------------------------------------------------------------------//

    public MiniMax(BoardEvaluator boardEvaluator) {
        this.boardEvaluator = new standardBoardEvaluator();
    }



    // Override methods ----------------------------------------------------------------------------------------------//


    /**
     * Returns the best move !
     * @param board l'echiquier
     * @param depth la profondeur de recherche
     * @return un String = les coordonnées x,y du bestMoove
     */
    @Override
    public String execute(PieceEchec[][] board, int depth) {

        final long startTime = System.currentTimeMillis();

        String bestMove = "";
        int lowestSeenValue = Integer.MAX_VALUE;
        int highestSeenValue = Integer.MIN_VALUE;
        int currentValue;
        int numMoves = 0;

        System.out.println("Current player : " + " THINKING with depth " + depth);

        // Pour tous les moves possibles du joueur
        for(Move move : Echiquier.getActivePlayer().getLegalMoves()) {

            // Execute chq move
            if(Echiquier.getActivePlayer().getCouleur() == 1) {

                // Ds la boucle ON définit la currentValue selon la couleur blanc = max et noirs = min
                // Au noirs de jouer
                currentValue =  min(board, depth -1);

                // Set lastColorPlayed ???

                if(currentValue <= lowestSeenValue) {
                    lowestSeenValue = currentValue;
                    bestMove = move.toString();
                }
            }
            else if (Echiquier.getActivePlayer().getCouleur() == 2) {

                // Au blancs de jouer
                currentValue =  max(board, depth -1);

                if(currentValue >= highestSeenValue) {
                    highestSeenValue = currentValue;
                    bestMove = move.toString();
                }
            }
        }

        final long executionTime = System.currentTimeMillis() - startTime;
System.out.println("Time elapsed : " + executionTime / 1000 + " seconds ");

        return bestMove;
    }




    // Private methods -----------------------------------------------------------------------------------------------//


    /**
     * Min : Execute chq move de chq piece puis compare la value pour la maj avec le max du noeud du dessous
     */
    public int min(final PieceEchec[][] board, final int depth){

        int lowestSeenValue = Integer.MAX_VALUE;

        if(depth == 0 /* Ou game over */) {
            return this.boardEvaluator.evaluate(board, depth);
        }

        for(Move move : Echiquier.getActivePlayer().getLegalMoves()) {

            if(Echiquier.getActivePlayer().makeMove(board, move)) {

                final int currentValue = max(board, depth -1);

                if(currentValue <= lowestSeenValue) {
                    lowestSeenValue = currentValue;
                }
            }
        }
        return lowestSeenValue;
    }





    public int max(final PieceEchec[][] board, final int depth){

        int highestSeenValue = Integer.MIN_VALUE;

        if(depth == 0 /* Ou game over */) {
            return this.boardEvaluator.evaluate(board, depth);
        }
        for(Move move : Echiquier.getActivePlayer().getLegalMoves()) {

            if(Echiquier.getActivePlayer().makeMove(board, move)) {

                final int currentValue = min(board, depth -1);

                if(currentValue >= highestSeenValue) {
                    highestSeenValue = currentValue;
                }
            }
        }
        return highestSeenValue;
    }





    @Override
    public String toString() {
        return "Mini Max";
    }





}
