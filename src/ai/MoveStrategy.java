package ai;


import pieces.PieceEchec;
import utilitaires.Echiquier;


public interface MoveStrategy {


    // Returns the best move
    String execute(PieceEchec[][] board, int depth);


}
