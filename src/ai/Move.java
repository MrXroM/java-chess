package ai;


import pieces.PieceEchec;
import utilitaires.Echiquier;

public class Move {


    // Properties ----------------------------------------------------------------------------------------------------//


    private PieceEchec movedPiece;
    private String destinationCoordz;
    private PieceEchec attackedPiece;


    // Constructors --------------------------------------------------------------------------------------------------//

    public Move( PieceEchec movedPiece,
                 String destinationCoordz) {

        this.movedPiece = movedPiece;
        this.destinationCoordz = destinationCoordz;
    }


    public Move( PieceEchec movedPiece,
                 String destinationCoordz, PieceEchec attackedPiece) {

        this.movedPiece = movedPiece;
        this.destinationCoordz = destinationCoordz;
        this.attackedPiece = attackedPiece;
    }


    // Getters / Setters ---------------------------------------------------------------------------------------------//

    public PieceEchec getMovedPiece() {
        return movedPiece;
    }
    public String getDestinationCoordz() {
        return destinationCoordz;
    }
    public PieceEchec getAttackedPiece() {
        return attackedPiece;
    }

    public void setMovedPiece(PieceEchec movedPiece) {
        this.movedPiece = movedPiece;
    }
    public void setDestinationCoordz(String destinationCoordz) {
        this.destinationCoordz = destinationCoordz;
    }
    public void setAttackedPiece(PieceEchec attackedPiece) {
        this.attackedPiece = attackedPiece;
    }


    // Private methods -----------------------------------------------------------------------------------------------//


    @Override
    public String toString() {
        return "Move is with this piece : " + movedPiece + " Going to this coordz : "
                + destinationCoordz + " And attacked piece is : " + attackedPiece + "\n";
    }










}
