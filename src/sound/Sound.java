package sound;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import java.io.InputStream;


public class Sound {


    public void playSound(String audioFile) {

        try {
            InputStream inputStream1 = getClass().getResourceAsStream(audioFile);

            AudioStream audioStream = new AudioStream(inputStream1);
            AudioPlayer.player.start(audioStream);
        } catch (Exception e) {
            // handle exception
            e.printStackTrace();
        }

    }




}
