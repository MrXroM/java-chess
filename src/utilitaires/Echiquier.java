package utilitaires;

import ai.Move;
import pieces.*;
import players.Player;
import java.util.ArrayList;
import java.util.Collection;



public class Echiquier {

    // Properties ----------------------------------------------------------------------------------------------------//


    private int nbPiecesRestantes = 32;
    private static PieceEchec [][] tEchiquier = new PieceEchec[8][8] ;
    private static PieceEchec [][] tEchiquierCopy = new PieceEchec[8][8] ;
    // les blancs & les noirs
    private static Collection<PieceEchec> whitePieces;
    private static Collection<PieceEchec> blackPieces;
    // Lists de moves possibles pour chq joueur
    private static Collection<Move> whiteLegalMoves;
    private static Collection<Move> blackLegalMoves;
    // La couleur qui a jouée le last moove
    // Par convention 1 les blancs // 2 les noirs
    private static int lastColorPlay = 2;

    // Les 2 rois
    private static Roi king1 = null;
    private static Roi king2 = null;
    // Attaquant adverse qui met un roi echec
    private static PieceEchec attaquantAdverse = null;
    // Pour le roque
    private static boolean rookBlancDone = false;
    private static boolean rookNoirDone = false;
    private static boolean smallRookBlancPossible = true;
    private static boolean bigRookBlancPossible = true;
    private static boolean smallRookNoirPossible = true;
    private static boolean bigRookNoirPossible = true;
    private static boolean tour1BlancAMoov = false;
    private static boolean tour2BlancAMoov = false;
    private static boolean tour1NoirAMoov = false;
    private static boolean tour2NoirAMoov = false;
    private static boolean roiBlancAMoov = false;
    private static boolean roiNoirAMoov = false;

    // Les joueurs
    private static Player player1 = new Player("Player 1", 1,false, whiteLegalMoves, blackLegalMoves, king1);  // Blancs
    private static Player player2 = new Player("Player 2", 2,false, whiteLegalMoves, blackLegalMoves, king2);

    // Le joueur qui doit jouer
    private static Player activePlayer = player1; // For first move

    // Methods -------------------------------------------------------------------------------------------------------//


    // Getter pour l'echiquier
    public static PieceEchec[][] getEchiquier() {
        return tEchiquier;
    }
    public static PieceEchec[][] getEchiquierCopy() {
        return tEchiquierCopy;
    }
    // Pour les listes des legal mooves de chq player
    public static Collection<Move> getWhiteLegalMoves() {
        return whiteLegalMoves;
    }
    public static Collection<Move> getBlackLegalMoves() {
        return blackLegalMoves;
    }
    // Pour les pieces restantes
    public static Collection<PieceEchec> getWhitePieces() {
        return whitePieces;
    }
    public static Collection<PieceEchec> getBlackPieces() {
        return blackPieces;
    }
    // Pour les kings
    public static Roi getWhiteKing() {
        return king1;
    }
    public static Roi getBlackKing() {
        return king2;
    }
    // Les joueurs
    public static Player getPlayer1() {
        return player1;
    }
    public static Player getPlayer2() {
        return player2;
    }

    // La lastColorPlayed
    public static int getLastColorPlay() {
        return lastColorPlay;
    }
    /**
     * Return Active player
     */
    public static Player getActivePlayer() {
        return activePlayer;
    }

    // Setters
    // pour la copie de l'echiquier
    public static void setEchiquierCopy(PieceEchec[][] tEchiquierCopy) {
        Echiquier.tEchiquierCopy = tEchiquierCopy;
    }
    // LastColorPlayed
    public static void setLastColorPlay(int lastColorPlay) {
        Echiquier.lastColorPlay = lastColorPlay;
    }



    /**
     * Place les pieces sur l'echiquer en début ou reset du jeu
     */
    public static void buildEchiquier() {

        // On clean tout d'abord (utile pour le reset)
        for(int i=0; i<8;i++){
            for(int j=0; j<8;j++) {
                tEchiquier[i][j] = null;
            }
        }
        resetRookFlags();

        // Pieces Blanches
        tEchiquier [0][0] = new Tour(0,0,2);
        tEchiquier [7][0] = new Tour(7,0,2);
        tEchiquier [1][0] = new Cavalier(1,0,2);
        tEchiquier [6][0] = new Cavalier(6,0,2);
        tEchiquier [2][0] = new Fou(2,0,2);
        tEchiquier [5][0] = new Fou(5,0,2);
        tEchiquier [3][0] = new Reine(3,0, 2);
        tEchiquier [4][0] = new Roi(4,0,2);
        // Pions
        for(int i = 0; i < 8; i++) {
            tEchiquier [i][1] = new Pion(i, 1, 2);
        }

        // Pieces Noires
        tEchiquier [0][7] = new Tour(0,7,1);
        tEchiquier [7][7] = new Tour(7,7,1);
        tEchiquier [1][7] = new Cavalier(1,7,1);
        tEchiquier [6][7] = new Cavalier(6,7,1);
        tEchiquier [2][7] = new Fou(2,7,1);
        tEchiquier [5][7] = new Fou(5,7,1);
        tEchiquier [3][7] = new Reine(3,7, 1);
        tEchiquier [4][7] = new Roi(4,7,1);
        // Pions
        for(int i = 0; i < 8; i++) {
            tEchiquier [i][6] = new Pion(i, 6, 1);
        }

        // Stoque les pieces blanches et noires
        whitePieces = calculateActivePieces(1);
        blackPieces = calculateActivePieces(2);
    }




    /**
     * Methode qui retourne un int à 1 ou 2
     * Si la pieces actuelle est sur une case blanche ou noire
     * @param positionX
     * @param positionY
     * @return 1 pour case noire, 2 pour case blanche
     */
    public static int getCouleurCase (int positionX, int positionY) {

        // Case X paire
        if(positionX % 2 == 0) {
            // Case Y paire >> Noire
            if(positionY % 2 == 0) {
                return 1;
            }
            else return 2;
        }
        else {
            // Case X impaire
            // Et Y impaire >> Noire
            if(positionY % 2 != 0) {
                return 1;
            }
            else return 2;
        }
    }






    /**
     * Set who has started
     * @param pieceActive the pA
     */
    public static void setGameBeginer(PieceEchec pieceActive){
        // Set qui a commencé
        if (player1.getMooves() == 0 && pieceActive.getCouleurPiece() == 1) {
            player1.setHasStarted(true);
        } else if (player2.getMooves() == 0 && pieceActive.getCouleurPiece() == 2) {
            player2.setHasStarted(true);
        }
    }





    /**
     * Incrémente le counter du joueur qui a move ou mangé
     * @param pe la pe active
     */
    public static void incrementCounters(PieceEchec pe) {
        if(pe.getCouleurPiece() == 1){
            player1.setMooves(player1.getMooves()+1);
        }
        else if(pe.getCouleurPiece() == 2) {
            player2.setMooves(player2.getMooves()+1);
        }
    }





    /**
     * Incremente le score du joueur qui mange une piece
     * @param pe la pe qui a bouffée
     */
    private static void incrementPlayerScore(PieceEchec pe) {
        if(pe.getCouleurPiece() == 1){
            player1.setScore(player1.getScore()+1);
        }
        else if(pe.getCouleurPiece() == 2) {
            player2.setScore(player2.getScore()+1);
        }
    }




    /**
     * Called at start or when a move is confirmed to update lists of allPossibleMoves
     */
    public static void moveConfirmed() {

        // Maj les tableaux des moves possibles de chq joueur
        whiteLegalMoves = calculateLegalMoves(1);
        blackLegalMoves = calculateLegalMoves(2);

System.out.println( "Player actif : " + getActivePlayer() + ">>>>>>>>>> WHITE POSSIBLE MOVES <<<<<<<<<<<<\n" + whiteLegalMoves.toString());
System.out.println( "Player actif : " + getActivePlayer() + ">>>>>>>>>> BLACK POSSIBLE MOVES <<<<<<<<<<<<\n" + blackLegalMoves.toString());
    }




    /**
     * Renvoi la piece qui est à la position xD, yD
     * @param xD coord X a check
     * @param yD coord Y a check
     * @return la piece sinon null
     */
    public static PieceEchec getPieceAt(int xD, int yD) {

        // Ici on va check en mm temps pour le roque si les pieces ont bougé
        checkRookConditions();

        // Maj les tableaux des pieces blanches et noires
        whitePieces = calculateActivePieces(1);
        blackPieces = calculateActivePieces(2);


        // Renvoi la piece présente sur la case x,y sinon null
        return tEchiquier[xD][yD];
    }



    /**
     * Calcul les pieces actives d'une couleur donnée en param
     * @param color int 1 les blancs et 2 les noirs
     * @return list des pieces
     */
    public static ArrayList<PieceEchec> calculateActivePieces (int color){

        ArrayList<PieceEchec> activePieces = new ArrayList<>();

        // Parcours de l'echiquier
        for(int i=0; i<8;i++){
            for(int j=0; j<8;j++) {

                if(tEchiquier[i][j] != null && tEchiquier[i][j].getCouleurPiece() == color){

                    activePieces.add(tEchiquier[i][j]);
                }
            }
        }
        return activePieces;
    }




    /**
     * Calcul tous les moves possbiles pour un joueur
      * @param color la couleur des pieces
     * @return
     */
    private static Collection<Move> calculateLegalMoves(int color) {

        ArrayList<Move> legalMoves = new ArrayList<>();


        ArrayList<PieceEchec> activePieces = new ArrayList<>();

        // Parcours de l'echiquier
        for(int i=0; i<8;i++){
            for(int j=0; j<8;j++) {

                if(tEchiquier[i][j] != null && tEchiquier[i][j].getCouleurPiece() == color){

                    // Re parcours de l'echiquier
                    for(int k=0; k<8; k++) {
                        for(int m=0; m<8; m++) {

                            // Formattage des coordz
                            String strDest = k + "," + m;

                            // Deplacement possible
                            if(getPieceAt(k,m) == null
                                    && tEchiquier[i][j].peutAllerA(k,m)){

                                // Add the move to list
                                legalMoves.add(new Move(tEchiquier[i][j], strDest));
                            }
                            // Manger une piece possible
                            else if(getPieceAt(k,m) != null
                                    &&  tEchiquier[i][j].peutMangerPiece(getPieceAt(k,m))) {

                                // Add the move to list
                                legalMoves.add(new Move( tEchiquier[i][j], strDest, tEchiquier[k][m]));
                            }
                        }
                    }
                }
            }
        }

        return legalMoves;
    }




    /**
     * Flag du roque
     */
    private static void checkRookConditions() {
        if(! roiBlancAMoov) {
            if(smallRookBlancPossible || bigRookBlancPossible){
                moovePiecesRook(1);
            }
        }
        if(! roiNoirAMoov){
            if(smallRookNoirPossible || bigRookNoirPossible){
                moovePiecesRook(2);
            }
        }
    }




    /**
     * Surveille et change la valeur des flags
     * si pieces requises pour le roque ont bougées
     * @param color
     */
    public static void moovePiecesRook(int color) {

        // Check pour les blancs --------------------------------------------
        if(color == 1) {
            // La tour blanche de gauche a bougée
            if(tEchiquier[0][7] == null){
                tour1BlancAMoov = true;
            } else {
                if(! tEchiquier[0][7].getClass().getName().equals("pieces.Tour")) {
                    tour1BlancAMoov = true;
                }
            }
            // La tour blanche de droite a bougée
            if(tEchiquier[7][7] == null) {
                tour2BlancAMoov = true;
            } else {
                if(! tEchiquier[7][7].getClass().getName().equals("pieces.Tour")) {
                        tour2BlancAMoov = true;
                }
            }
            // le roi blanc a bougé
            if(tEchiquier[4][7] == null) {
                roiBlancAMoov = true;
            } else {
                if(! tEchiquier[4][7].getClass().getName().equals("pieces.Roi")) {
                    roiBlancAMoov = true;
                }
            } // Check final
            if(roiBlancAMoov || tour1BlancAMoov) {
                bigRookBlancPossible = false;
            }
            if(roiBlancAMoov ||tour2BlancAMoov) {
                smallRookBlancPossible = false;
            }

        } else {
            // Check pour les noirs ---------------------------------------------
            // La tour noire de gauche a bougée
            if(tEchiquier[0][0] == null) {
                tour1NoirAMoov = true;
            } else {
                if(! tEchiquier[0][0].getClass().getName().equals("pieces.Tour")) {
                    tour1NoirAMoov = true;
                }
            }
            // La tour noire de gauche a bougée
            if(tEchiquier[7][0] == null) {
                tour2NoirAMoov = true;
            } else {
                if(! tEchiquier[7][0].getClass().getName().equals("pieces.Tour")) {
                    tour2NoirAMoov = true;
                }
            }
            // La tour noire de gauche a bougée
            if(tEchiquier[4][0] == null) {
                roiNoirAMoov = true;
            } else {
                if(! tEchiquier[4][0].getClass().getName().equals("pieces.Roi")) {
                    roiNoirAMoov = true;
                }
            }
            if(roiNoirAMoov || tour1NoirAMoov) {
                bigRookNoirPossible = false;
            }
            if(roiNoirAMoov || tour2NoirAMoov) {
                smallRookNoirPossible = false;
            }

        }
    }





    /**
     * Reset les flags en cas de reset de partie
     */
    public static void resetRookFlags(){
        tour1BlancAMoov = false;
        tour2BlancAMoov = false;
        roiBlancAMoov = false;
        tour1NoirAMoov = false;
        tour2NoirAMoov = false;
        roiNoirAMoov = false;
        smallRookBlancPossible = true;
        bigRookBlancPossible = true;
        smallRookNoirPossible = true;
        bigRookNoirPossible = true;
    }






    /**
     * Check si le rook est possible 3 conditions
     * Roi pas en echec
     * Aucune piece n'a bougée
     * Pas d'echec sur les 2 cases pour faire le rook
     * @param king le roi a test avec sa position sa couleur
     * @param xD coord X ou le roi veut aller
     * @param yD coord Y ou le roi veut aller
     * @return True si Ok
     */
    public static boolean isRookPossible(PieceEchec[][]echiquier, PieceEchec king, int xD, int yD) {

        boolean result = false;
        boolean isCasesOccup = false;
        boolean isEchecHere = false;
        PieceEchec roiPos1 = null;
        PieceEchec roiPos2 = null;

        if(! rookBlancDone) {

            if(king.getCouleurPiece() == 1) {
                if(isInCheck(echiquier) !=1){

                    // Petit roque
                    if(xD == 6 && yD == 7) {
                        if(smallRookBlancPossible) {
                            // Check si les cases sont vides
                            if(echiquier[xD -1][yD] != null && echiquier[xD][yD] != null) {
                                isCasesOccup = true;
                            }
                            // Sur chacune des 2 case si une piece met le roi en echec
                            roiPos1 = new Roi(xD -1, yD,1);
                            roiPos2 = new Roi(xD, yD, 1);

                            for(int i=0; i<8; i++){
                                for(int j=0; j<8; j++){
                                    if(echiquier[i][j] != null && echiquier[i][j].getCouleurPiece() != 1
                                            && ( echiquier[i][j].peutMangerPiece(roiPos1) || echiquier[i][j].peutMangerPiece(roiPos2))) {
                                        isEchecHere = true;
                                    }
                                }
                            }
                            if(! isCasesOccup && !isEchecHere) {
                                result = true;
                            }
                        }
                    }
                    // Grand roque
                    if(xD == 2 && yD == 7) {
                        if(bigRookBlancPossible) {
                            // Check cases vides
                            if(echiquier[xD -1][yD] != null && echiquier[xD][yD] != null
                                    && echiquier[xD + 1][yD] != null) {
                                isCasesOccup = true;
                            }
                            // Echec ou non sur les 2 cases du trajet du rook
                            roiPos1 = new Roi(xD, yD, 2);
                            roiPos2 = new Roi(xD +1, yD,2);

                            for(int i=0; i<8; i++){
                                for(int j=0; j<8; j++){
                                    if(echiquier[i][j] != null && echiquier[i][j].getCouleurPiece() != king.getCouleurPiece()
                                            && ( echiquier[i][j].peutMangerPiece(roiPos2) || echiquier[i][j].peutMangerPiece(roiPos1))) {
                                        isEchecHere = true;
                                    }
                                }
                            }
                            if(! isCasesOccup && !isEchecHere) {
                                result = true;
                            }
                        }
                    }
                }
            }
        }

        if(! rookNoirDone) {
            if(king.getCouleurPiece() == 2){
                if(isInCheck(echiquier) !=2){

                    // Petit rook
                    if (smallRookNoirPossible) {
                        if(xD == 6 && yD == 0){
                            // Cases vides
                            if(echiquier[xD -1][yD] != null && echiquier[xD][yD] != null) {
                                isCasesOccup = true;
                            }
                            // Sur chacune des 2 case si une piece met le roi en echec
                            roiPos1 = new Roi(xD , yD,2);
                            roiPos2 = new Roi(xD -1, yD, 2);

                            for(int i=0; i<8; i++){
                                for(int j=0; j<8; j++){
                                    if(echiquier[i][j] != null && echiquier[i][j].getCouleurPiece() != 2
                                            && ( echiquier[i][j].peutMangerPiece(roiPos1) || echiquier[i][j].peutMangerPiece(roiPos2))) {
                                        isEchecHere = true;
                                    }
                                }
                            }
                            if(! isCasesOccup && !isEchecHere) {
                                result = true;
                            }
                        }
                    }

                    // Grand rook
                    if(xD ==2 && yD == 0){
                        if(bigRookNoirPossible) {
                            // Cases vides
                            if(echiquier[xD -1][yD] != null && echiquier[xD + 1][yD] != null
                                    && echiquier[xD][yD] != null) {
                                isCasesOccup = true;
                            }
                            // Sur chacune des 2 case si une piece met le roi en echec
                            roiPos1 = new Roi(xD +1, yD,2);
                            roiPos2 = new Roi(xD, yD, 2);

                            for(int i=0; i<8; i++){
                                for(int j=0; j<8; j++){
                                    if(echiquier[i][j] != null && echiquier[i][j].getCouleurPiece() != 2
                                            && ( echiquier[i][j].peutMangerPiece(roiPos2) || echiquier[i][j].peutMangerPiece(roiPos1))) {
                                        isEchecHere = true;
                                    }
                                }
                            }
                            if(! isCasesOccup && !isEchecHere) {
                                result = true;
                            }
                        }
                    }
                }
            }
        }
        return result;
    }






    /**
     * Do le move dans l'echiquier / deplacement ou manger une piece
     * @param pieceActive la pA
     * @param posX coord destination x
     * @param posY destination y
     */
    public static void dotheMove(PieceEchec[][] echiquier,  PieceEchec pieceActive, int posX, int posY) {

        // Deplacement
        if(echiquier[posX][posY] == null) {

            // null sur l'ancienne place
            echiquier[pieceActive.getPositionX()][pieceActive.getPositionY()] = null;
            // Change les attr de l'objet
            pieceActive.setPositionX(posX);
            pieceActive.setPositionY(posY);
            // Ds le tab Echiquier  Moove a la new place
            echiquier[posX][posY] = pieceActive;

            // Definit le beginner du game
            setGameBeginer(pieceActive);
            // increment counters
            incrementCounters(pieceActive);
        }
        // Mange une piece
        else {
            // Supprime la piece victime
            echiquier[posX][posY] = null;
            // null sur l'ancienne place
            echiquier[pieceActive.getPositionX()][pieceActive.getPositionY()] = null;
            // Change les attr de l'objet
            pieceActive.setPositionY(posX);
            pieceActive.setPositionX(posY);
            // Ds le tab Echiquier  Moove a la new place
            echiquier[posX][posY] = pieceActive;

            // increment counters
            incrementCounters(pieceActive);
            incrementPlayerScore(pieceActive);
        }
    }








    /**
     * Execute le rook sur l'echiquier
     * @param king le roi
     * @param xD coord X
     * @param yD coord Y
     */
    public static void doTheRook(PieceEchec king, int xD, int yD){

        PieceEchec tempPiece = null;

        if(king.getCouleurPiece() == 1) {
            // petit rook
            if(king.getPositionX() == 4 && xD == 6) {
                // On bouge le king de 2 cases à droite
                tEchiquier[king.getPositionX()][king.getPositionY()] = null;
                king.setPositionX(6);
                tEchiquier[6][7] = king;
                // On bouge la tour de 2 cases à gauche
                tempPiece = tEchiquier[7][7];
                tempPiece.setPositionX(5);
                tEchiquier[7][7] = null;
                tEchiquier[5][7] = tempPiece;
            }
            // Grand rook
            if(king.getPositionX() == 4 && xD == 2) {
                // Bouge le king a gauche de 2 cases
                tEchiquier[king.getPositionX()][king.getPositionY()] = null;
                king.setPositionX(2);
                tEchiquier[2][7] = king;
                // Bouge la tour
                tempPiece = tEchiquier[0][7];
                tempPiece.setPositionX(3);
                tEchiquier[0][7] = null;
                tEchiquier[3][7] = tempPiece;
            }
            // var globale maj
            rookBlancDone = true;
        }
        if(king.getCouleurPiece() == 2) {
            // petit rook
            if(king.getPositionX() == 4 && xD == 6) {
                tEchiquier[king.getPositionX()][king.getPositionY()] = null;
                king.setPositionX(6);
                tEchiquier[6][0] = king;
                // On bouge la tour de 2 cases à gauche
                tempPiece = tEchiquier[7][0];
                tempPiece.setPositionX(5);
                tEchiquier[7][0] = null;
                tEchiquier[5][0] = tempPiece;
            }

            if(king.getPositionX() == 4 && xD == 2) {
                tEchiquier[king.getPositionX()][king.getPositionY()] = null;
                king.setPositionX(2);
                tEchiquier[2][0] = king;
                tempPiece = tEchiquier[0][0];
                tempPiece.setPositionX(3);
                tEchiquier[0][0] = null;
                tEchiquier[3][0] = tempPiece;
            }
            rookNoirDone = true;
        }

    }





    /**
     * Parcours l'echiquier et défini les 2 kings ds des var globales
     */
    public static void getTheKings() {
        // Défini les 2 Kings
        for(int i =0; i < 8; i++){
            for(int j=0; j < 8; j++) {
                if(Echiquier.getPieceAt(i, j) != null) {
                    if (Echiquier.getPieceAt(i,j).getClass().getName().compareTo("pieces.Roi") == 0) {
                        // Set les kings B & N
                        if(Echiquier.getPieceAt(i,j).getCouleurPiece() == 1) {
                            king1 = (Roi) Echiquier.getPieceAt(i, j);
                        }
                        if(Echiquier.getPieceAt(i,j).getCouleurPiece() == 2) {
                            king2 = (Roi) Echiquier.getPieceAt(i, j);
                        }
                   }
                }
            }
        }
    }






    /**
     * Calcule et stocke les cases vides dans la trajectoire entre la piece attaquante et le roi echec
     * @param kingVictim le roi echec
     * @param attaquant l'attaquant
     * @return l'arraylist avec les coordonnées X et Y des cases vides
     */
    public static ArrayList<String> getCasesTrajCheck(PieceEchec kingVictim, PieceEchec attaquant) {

        ArrayList<String> listCases = new ArrayList<>();
        int victimPosX = kingVictim.getPositionX();
        int victimPosY = kingVictim.getPositionY();
        int attaqPosX = attaquant.getPositionX();
        int attaqPosY = attaquant.getPositionY();

        int trajAttaqX = attaqPosX - victimPosX;
        int trajAttaqY = attaqPosY - victimPosY;

        // 2 types d'attaques : En ligne et en diago
        // En diago -----------------------------------------------------------------------------//
        if(Math.abs(trajAttaqX) == Math.abs(trajAttaqY)) {

            // Moove en haut à gauche -------------------------------------
            if(trajAttaqX > 0 && trajAttaqY > 0) {

                // Check chq case
                for(int x=attaqPosX -1, y=attaqPosY -1; x>=victimPosX+1; x--, y--) {
                    String msg = "";
                    // Case vide On mémorise ses coordz ds l'arrayList
                    if(getPieceAt(x,y) == null) {
                        msg = x + "," + y;
                        listCases.add(msg);
                    }
                }
            }
            // Moove en haut à droite -------------------------------------
            if(trajAttaqX < 0 && trajAttaqY > 0) {

                for(int x=attaqPosX +1, y=attaqPosY -1; x<=victimPosX -1; x++, y--) {
                    String msg = "";
                    if(getPieceAt(x,y) == null) {
                        msg = x + "," + y;
                        listCases.add(msg);
                    }
                }
            }
            // Moove en bas à gauche -------------------------------------
            if(trajAttaqX > 0 && trajAttaqY < 0) {

                for(int x=attaqPosX -1, y=attaqPosY +1; x>=victimPosX+1; x--,  y++) {
                    String msg = "";
                    if(getPieceAt(x,y) == null) {
                        msg = x + "," + y;
                        listCases.add(msg);
                    }
                }
            }
            // Moove en bas à droite -------------------------------------
            if(trajAttaqX < 0 && trajAttaqY < 0) {

                for(int x=attaqPosX +1, y=attaqPosY +1; x<=victimPosX-1; x++, y++) {
                    String msg = "";
                    if(getPieceAt(x,y) == null) {
                        msg = x + "," + y;
                        listCases.add(msg);
                    }
                }
            }

        } else {
            // En ligne -----------------------------------------------------------------------------//
            // Moove en haut -------------------------------------
            if(trajAttaqX == 0 && trajAttaqY >0) {

               for(int y=attaqPosY -1; y>=victimPosY+1; y--) {
                    String msg = "";
                   if(getPieceAt(attaqPosX,y) == null) {
                       msg = attaqPosX + "," + y;
                       listCases.add(msg);
                   }
               }
            }
            // Moove en bas -------------------------------------
            if(trajAttaqX == 0 && trajAttaqY <0) {

                for(int y=attaqPosY +1; y<=victimPosY-1; y++) {
                    String msg = "";
                    if(getPieceAt(attaqPosX,y) == null) {
                        msg = attaqPosX + "," + y;
                        listCases.add(msg);
                    }
                }
            }
            // Moove a gauche ------------------------------------
            if(trajAttaqY == 0 && trajAttaqX >0) {

                for(int x=attaqPosX -1, y=attaqPosY; x>=victimPosX+1;x--) {
                    String msg = "";
                    if(getPieceAt(x,y) == null) {
                        msg = x + "," + y;
                        listCases.add(msg);
                    }
                }
            }
            // Moove a droite ------------------------------------
            if(trajAttaqY == 0 && trajAttaqX <0) {

                for(int x=attaqPosX +1, y=attaqPosY; x<=victimPosX-1; x++) {
                    String msg = "";
                    if(getPieceAt(x,y) == null) {
                        msg = x + "," + y;
                        listCases.add(msg);
                    }
                }
            }

        }
        return listCases;
    }






    /**
     * Check si le moove met sa couleur echec pour l'invalider (rook séparé return 0)
     * Check si le moove du roi Echec le sort de l'echec pour le valider
     * Simule le moove / manger piece sur l'echiquier pr test avec kingIsCheck()
     * @param peActive la piece a test
     * @param x coord X
     * @param y coord Y
     * @return 0 si le moove ne met pas echec
     *  1 si le moove met echec les blancs et 2 pour les noirs
     */
    public static int checkGoodMoove(PieceEchec[][] echiquier, PieceEchec peActive, int x, int y) {

        // Sauvegarde de la position initiale de la piece
        int posX = 0;
        int posY = 0;
        // int to send
        int toSend = 0;
        // Piece victim
        PieceEchec victim = null;

        // Pour les 2 couleurs
        for(int i =1; i<=2; i++) {

            if(peActive.getCouleurPiece() == i ) {

                // Pour tous les mooves sauf le rook ou en renvoi 0 car la methode isRookPossible check le reste
                if(peActive.getClass().getName().equals("pieces.Roi")
                        && Math.abs(peActive.getPositionX() - x) == 2
                        && peActive.getPositionX() == 4
                        && ( peActive.getPositionY() == 7
                        || peActive.getPositionY() == 0) ) {
                    return 0;
                }

                // Aucune piece présente
                // Pour deplacer une piece ----------------------------------------- //
                if(echiquier[x][y] == null){

                    if(peActive.peutAllerA(x,y)) {

                        // Sauvegarde la postion initiale
                        posX = peActive.getPositionX();
                        posY = peActive.getPositionY();

                        // On fait le moove
                        echiquier[peActive.getPositionX()][peActive.getPositionY()] = null;
                        peActive.setPositionY(y);
                        peActive.setPositionX(x);
                        echiquier[x][y] = peActive;

                        // On check
                        if(isInCheck(echiquier) == i) {
                            toSend = i;
                        }
                        // on annule le moove
                        echiquier[x][y] = null;
                        peActive.setPositionX(posX);
                        peActive.setPositionY(posY);
                        echiquier[peActive.getPositionX()][peActive.getPositionY()] = peActive;
                    }
                }
                // Pour manger une piece --------------------------------------------- //
                else {
                    // Simule peutMangerPiece(pe)
                    victim = getPieceAt(x,y);

                    // la position de la piece qui mange
                    posX = peActive.getPositionX();
                    posY = peActive.getPositionY();

                    // Piece Active tout sauf un roi (moove special)
                    if(peActive.peutMangerPiece(victim) && ! peActive.getClass().getName().equals("pieces.Roi")) {

                        // Do the moove
                        echiquier[x][y] = null;
                        echiquier[peActive.getPositionX()][peActive.getPositionY()] = null;
                        peActive.setPositionX(x);
                        peActive.setPositionY(y);
                        echiquier[x][y] = peActive;

                        // Check si toujours echec
                        if(isInCheck(echiquier) == i) {
                            toSend = i;
                        }
                        // Annule le moove
                        peActive.setPositionY(posY);
                        peActive.setPositionX(posX);
                        echiquier[x][y] = victim;
                        echiquier[posX][posY] = peActive;
                    }
                    // Roi qui veut manger
                    if(peActive.peutMangerPiece(victim) && peActive.getClass().getName().equals("pieces.Roi")) {

                        // Simule le moove du roi qui mange
                        // Check si une piece peut le manger si oui le moove est invalidé

                        // Do the moove.
                        echiquier[peActive.getPositionX()][peActive.getPositionY()] = null;
                        echiquier[x][y] = null;
                        peActive.setPositionX(x);
                        peActive.setPositionY(y);
                        echiquier[x][y] = peActive;

                        // Check si une piece adverse peut le manger
                        for(int e=0;e<8;e++) {
                            for(int f=0;f<8;f++) {
                                if(echiquier[e][f] != null && echiquier[e][f].getCouleurPiece() != peActive.getCouleurPiece()){
                                    PieceEchec pieceAdv = echiquier[e][f];
                                    if(pieceAdv.peutMangerPiece(peActive)) {
                                        // On invalide le moove
                                        toSend = i;
                                    }
                                }
                            }
                        }

                        // Check si toujours echec
                        if(isInCheck(echiquier) == i) {
                            toSend = i;
                        }
                        // Annule le moove
                        peActive.setPositionX(posX);
                        peActive.setPositionY(posY);
                        echiquier[x][y] = victim;
                        echiquier[posX][posY] = peActive;
                    }
                }
            }
        }
        return toSend;
    }





    /**
     * Un roi est echec ?
     * @return 0 aucun, 1 le blanc et 2 le noir
     */
    public static int isInCheck(PieceEchec[][] echiquier) {

        // int renvoyé par la methode
        int kingEchec = 0;

        // Passe ttes les cases de l'echiquier
        for(int k = 0; k < 8; k++){
            for(int m=0; m < 8; m++) {
                // Check si ttes les pieces noires de l'echiquier peuvent mettre echec le roi blanc
                if(echiquier[k][m] != null
                        && echiquier[k][m].getCouleurPiece() != king1.getCouleurPiece()) {
                    if(echiquier[k][m].peutMangerPiece(king1)) {
                        attaquantAdverse = echiquier[k][m];
                        kingEchec = 1;
                    }
                }
                // Check si ttes les pieces blanches de l'echiquier peuvent mettre echec le roi noir
                if(echiquier[k][m] != null
                        && echiquier[k][m].getCouleurPiece() != king2.getCouleurPiece()
                        && echiquier[k][m].peutMangerPiece(king2)) {
                    attaquantAdverse = echiquier[k][m];
                    kingEchec = 2;
                }
            }
        }
        return kingEchec;
    }







    /**
     * Check si un roi est echec ou echec et mat 3 conditions :
     * 1. Si une piece peut manger l'attaquant
     * 2. Si une piece peut se mettre en barage de la trajectoire de l'attaquant.
     * Appel la fonction getCasesTrajCheck() qui renvoi un ArrayList avec les coordz des cases de la trajectoire d'attaque.
     * Puis test de chacune de mes pieces avec peutAllerA() chaque case de la trajectoire
     * 3. Si le roi ne peut pas aller sur les cases autour sans etre echec :
     * Compte le nb de cases vides autour de lui ou il pourrait aller (positionsToCheck)
     * Après le test de ttes les pieces adverses ds chq position / si il est bouffé on incrémente positionNotSafe
     * On compare les 2 compteurs : Si = il est echec et mat sinon une position possible : juste échec
     * @return 0 si aucun echec et mat / 1 si le roi Blanc est echec et mat / 2 si le roi noir est echec et mat
     */
    public static int aKingIsCheckMate(PieceEchec[][] echiquier) {

        // Variables
        // int renvoyé par la methode
        int kingEchecOuMat = 0;
        boolean attaquantCanBeEat = false;
        boolean attaquantTrajCanBeCut = false;
        // roi victim
        PieceEchec victimKing = null;
        int posX = 0;
        int posY = 0;
        int positionsToCheck = 0;
        int positionsNotSafe = 0;
        boolean kingSafe;

        // ----------------------------Echec et mat --------------------------------------------------
        // Roi blanc echec et mat --------------------------------------------//
        if(isInCheck(echiquier) == 1 && attaquantAdverse != null) {

            // 1. On doit check si l'attaquant peut etre mangé par une piece -------------------------
            for(int p=0; p<8;p++){
                for(int q=0;q<8;q++){
                    if(echiquier[p][q] != null && echiquier[p][q].getCouleurPiece() == king1.getCouleurPiece()
                        && echiquier[p][q].peutMangerPiece(attaquantAdverse)) {
                        attaquantCanBeEat = true;
                    }
                }
            }

            //2. On doit check si la trajectoire de l'attaquant peut etre coupée par une piece ------
            // NE peut etre qu'un fou, tour ou reine
            if(attaquantAdverse.getClass().getName().equals("pieces.Fou") ||
                attaquantAdverse.getClass().getName().equals("pieces.Tour") ||
                attaquantAdverse.getClass().getName().equals("pieces.Reine") ) {

                // les cases vides entre l'attaquant et le king
                ArrayList<String> casesToCheck = getCasesTrajCheck(king1, attaquantAdverse);

                // On check chaque case vide ds la trajectoire avec chacune de nos pieces
                for(String caseVal : casesToCheck){

                    // Recup les coordz X et Y
                    int caseX = Integer.parseInt(caseVal.split(",")[0]);
                    int caseY = Integer.parseInt(caseVal.split(",")[1]);

                    // On check si chacune de nos pieces peut aller sur une de ses cases vides
                    for(int r=0; r<8;r++){
                        for(int s=0;s<8;s++){
                            if( echiquier[r][s] != null && echiquier[r][s].getCouleurPiece() == king1.getCouleurPiece()
                                    && echiquier[r][s].peutAllerA(caseX,caseY)) {
                                // Une fois suffit
                                attaquantTrajCanBeCut = true;
                            }
                        }
                    }
                }
            }

            // 3. On doit check toutes les positions vides autour du king echec  --------------------
            // Coordz -1 a +1 pour test les positions autour du king
            posX = king1.getPositionX();
            posY = king1.getPositionY();

            for(int x = posX -1; x <= posX +1; x++) {
                for(int y= posY -1; y <= posY +1; y++) {

                    // Dans le range de l'echiquier et case vide
                    if( (x >= 0 && x < 8) && (y>=0 && y<8) && echiquier[x][y] == null) {

                        positionsToCheck +=1;

                        // Reset le flag
                        kingSafe = true;

                        // Check avec le Roi partout autour de sa position initiale
                        victimKing = new Roi(x, y, king1.getCouleurPiece());

                        // Pour chaque position du roi, check de ttes les pieces adverses avec peutManger victimKing
                        for(int i=0; i<8; i++){
                            for(int j=0; j<8; j++) {
                                // la piece qu'on test
                                PieceEchec pieceAdverse = echiquier[i][j];

                                if(pieceAdverse != null && pieceAdverse.getCouleurPiece() != victimKing.getCouleurPiece()
                                        && pieceAdverse.peutMangerPiece(victimKing) ) {

                                    // Une piece adverse peut le manger ici
                                   kingSafe = false;
                                }
                            }
                        }
                        // Une position est non safe
                        if( ! kingSafe) {
                            positionsNotSafe += 1;
                        }
                    }
                }
            }
            // Fin du check de ttes les positions avec ttes les pieces adverses
            if(positionsToCheck == positionsNotSafe && ! attaquantCanBeEat && ! attaquantTrajCanBeCut) {
                kingEchecOuMat = 1;
            }
        }

        // Roi Noir echec et mat --------------------------------------------//
        if(isInCheck(echiquier) == 2 && attaquantAdverse != null){

            // 1.Check 1 If attaquant mangé par une piece -------------------------
            for(int p=0; p<8;p++){
                for(int q=0;q<8;q++){
                    if(echiquier[p][q] != null
                        && echiquier[p][q].peutMangerPiece(attaquantAdverse)
                        && echiquier[p][q].getCouleurPiece() == king2.getCouleurPiece()) {
                        attaquantCanBeEat = true;
                    }
                }
            }

            // Check 2 If la trajectoire de l'attaquant peut etre coupée par une piece -----
            if(attaquantAdverse.getClass().getName().equals("pieces.Tour") ||
                    attaquantAdverse.getClass().getName().equals("pieces.Fou") ||
                    attaquantAdverse.getClass().getName().equals("pieces.Reine") ) {

                // les cases vides entre l'attaquant et le king
                ArrayList<String> casesToCheck = getCasesTrajCheck(king2, attaquantAdverse);

                // On check chaque case vide ds la trajectoire avec chacune de nos pieces
                for(String caseVal : casesToCheck){

                    // Recup les coordz X et Y
                    int caseY = Integer.parseInt(caseVal.split(",")[1]);
                    int caseX = Integer.parseInt(caseVal.split(",")[0]);


                    // On check si chacune de nos pieces peut aller sur une de ses cases vides
                    for(int r=0; r<8;r++){
                        for(int s=0;s<8;s++){
                            if(echiquier[r][s] != null
                                && echiquier[r][s].getCouleurPiece() == king2.getCouleurPiece()
                                && echiquier[r][s].peutAllerA(caseX,caseY)) {
                                attaquantTrajCanBeCut = true;
                            }
                        }
                    }
                }
            }

            // Check 3 If case vide sans danger pour le king autour de lui
            posX = king2.getPositionX();
            posY = king2.getPositionY();

            // Coordz -1 a +1 pour test les positions autour du king
            for(int g = posX -1; g <= posX +1; g++) {
                for(int h= posY -1; h <= posY +1; h++) {

                    // Case vide autour du roi
                    if((g >= 0 && g < 8) && (h>=0 && h<8) && echiquier[g][h] == null) {

                        positionsToCheck +=1;
                        kingSafe = true;
                        // New king
                        victimKing = new Roi(g, h, king2.getCouleurPiece());

                        // Check si Echec et mat
                        for(int k = 0; k < 8; k++) {
                            for(int m = 0; m < 8; m++) {
                                PieceEchec pieceAdverse = echiquier[k][m];

                                if(pieceAdverse != null && pieceAdverse.getCouleurPiece() != 2
                                        && pieceAdverse.peutMangerPiece(victimKing)) {
                                    kingSafe = false;
                                }
                            }
                        }
                        // Une position est non safe
                        if( ! kingSafe) {
                            positionsNotSafe += 1;
                        }
                    }
                }
            }
            // Fin du check de ttes les positions avec ttes les pieces adverses
            if(positionsToCheck == positionsNotSafe && ! attaquantTrajCanBeCut && ! attaquantCanBeEat) {
                kingEchecOuMat = 2;
            }
        }
        victimKing = null;
        return kingEchecOuMat;
    }










}
